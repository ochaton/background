# Background

Library to simple spawn fault-tolerant background Fiber

- [Background](#background)
  - [Why do I need background?](#why-do-i-need-background)
  - [Background configuration](#background-configuration)
  - [Installation](#installation)
  - [Usage](#usage)
  - [Some examples](#some-examples)
    - [Example: ping fiber](#example-ping-fiber)
    - [Example: sync fiber](#example-sync-fiber)
    - [Example only replica reloadable fiber](#example-only-replica-reloadable-fiber)
  - [Example obtain state of previous job](#example-obtain-state-of-previous-job)

## Why do I need background?

You need `background` if you have to spawn background tarantool fiber to perform long running job.

It is usefull when:

1. You need your background fiber running only on replica or only on master of replicaset and you don't want to deal with manual/automatic failover
2. You need your job executed inifinite times with sleep control between runs
3. You have `package.reload` and you want your fibers be reloadable
4. You want support of gracefull shutdown for your background fibers
5. You want to proper replace previous background fiber with the new one and not deal with races
6. You want to wakeup you background fiber from other fibers to process new coming data or to redo it's job
7. You want your fibers to be restarted when they unexpectedly failed
8. You want to grab statistics of your background fiber lifetime from single entrypoint
9. You want to monitor your fibers activity when their execution unexpectedly failed

## Background configuration

`background` makes neccessary wrappings around tarantool fiber and returns `job` object to deal with.

Each job has lots of optional configurations to give you exactly the fiber you need.

job has exactly one mandatory parameter `func` which will be executed over and over.

| option | mandatory     | default               | Type                    | description              |
|--------|---------------|-----------------------|-------------------------|--------------------------|
| `func` | **mandatory** | none | `fun(job: background, ...:args): sleep_interval?` | main function of the job. if numeric value is returned, then next `sleep_interval` will be adjusted |
| `name` | optional      | `background/<number>` | string                  | the name of your fiber   |
| `wait` | optional      | `false`               | enum: "ro"/"rw"         | ask job to wait before node became ro or rw (by default waits nothing) |
| `run_interval` | optional | 1 second | number (seconds, fractional) | Sleep time interval between subsequent executions of `func` |
| `restart` | optional | `false` | boolean/number (seconds, fractional) | if truthy waits after failure before start executions again. If **true** is passed, then sleep will be 1 second |
| `once` | optional | `false` | boolean | if true then job will be permanently finished after single successfull execution of `func`. If `restart` is defined then job will be restarted only when execution of `func` was previously failed |
| `eternal` | optional | `true` | boolean | makes sence only with `package.reload` installed. when `false` gracefully shutdowns execution after code was reloaded by `package.reload` |
| `replace` | optional | none | `background job` | if defined then gracefully shutdowns given job and enters main loop only after `replace` job is finished |
| `replace_timeout` | optional | 60 seconds | number (seconds, fractional) | gracefull timeout between first and second shutdown of `replace job`. second shutdown will perform `fiber.cancel()` on `replace job` |
| `initialize` | optional | `log.info` | `fun(job: background, has_state: boolean?, state_or_err:any)` | callback which is called exactly once right before previous job was `replaced`. pass state or error from previous job `teardown` if was any |
| `teardown` | optional | `log.info` | `fun(job: background): state:any` | final callback which is called before fiber will be completely finished. allows to export state from here to pass it to `replacing job`. Guaranteed to be executed even if `fiber.cancel()` was called |
| `setup` | optional | `log.info` | `fun(job: background)` | callback is called after each restart or first start. It is called after `initialize` but before `run_while`. If `setup` raises an exception job will be permanently aborted |
| `run_while` | optional | `internal func` | `fun(job: background): boolean` | callback is called after `setup` but before each subsequent `func` execution. if callback does not return `truthy` then job will be permanently finished. If callback raises an exception job will be restarted if `restart` is defined. Usefull to check predicates before execution of `func` |
| `on_fail` | optional | `log.error` |  `fun(job: background, err:any)` | fail callback which is called when `func`, `setup`, `teardown`, `initialize` callbacks raises an exception. If `on_fail` reraises exception from `initialize` then job will be permanently finished |
| `args` | optional | {} | []any | list of constant arguments which will be passed to `func` on each execution |

## Installation

```bash
tarantoolctl rocks --server https://rocks.ochaton.me background
```

If you have Tarantool 2.10+ just add `https://rocks.ochaton.me` to your `rocks_servers` config

```bash
$ cat .rocks/config-5.1.lua
rocks_servers = {
    "https://rocks.ochaton.me", -- ochaton libs
    "http://moonlibs.github.io/rocks", -- moonlibs libs
    "http://rocks.tarantool.org/", -- tarantool libs
    "http://luarocks.org/repositories/rocks", -- luarocks
}
```

Then you may install lib to project with

```bash
tarantoolctl rocks background
```

## Usage

Background aims to simplify work with eternal while-loop single fiber.

```lua
local my_job_f = require 'background' {
    name = 'notifications',
    wait = false, -- possibilities: rw, ro
    run_interval = 0.5, -- fiber.sleep(run_interval) between consecutive executions
    args = {}, -- arguments of the call. Better to be constant. Way to pass constant args to fiber
    eternal = false, -- should fiber live across reloads or not (needs package.reload). default=false.
    restart = false, -- default: false (if restart=true, fiber will be restarted after 1s, you can specify restart timout setting restart=<timeout>).
    setup = function(job)
        -- setup callback is called each time fiber is initiated. (before wait=rw)
        job.run_deadline = fiber.time()+3600
    end,
    teardown = function(job)
        -- Callback is called when fiber is stopped and will not be restarted
    end,
    run_while = function(job)
        -- Callback is called each time before `func`. If returns not truthy then fiber will be stopped
        -- And not restarted.
        -- It is cleanest way to stop fiber from the inside of the fiber.
        -- By default: checks package.reload.count if package.reload is installed.
        -- If you pass `run_while` callback you must check package.reload.count on yout own.
        return fiber.time() < job.run_deadline
    end,
    func = function(job, ...)
        -- main loop. You may here write your logic
    end,
    on_fail = function(job, err)
        -- callback is called each time `func` raises an error.
        -- you may log it or send metrics here.
        -- If `func` or `on_fail` raises and error job will be restarted only if `restart` is enabled
        -- You may call job:shutdown() to stop fiber (and it will not be restarted). But that would be unusual
    end,
    once = false, -- you may spawn fiber for only single call of `func`. `restart` will restart it only if execution was failed.
}

-- You may wakeup `my_job_f` explicitly outside of the fiber
function recount()
    my_job_f:notify() -- notify will wakeup fiber if it is sleeping between `exec`. It safe to call if fiber `waits` or cancelled.
end

function stop()
    my_job_f:shutdown() -- gracefully shutdowns fiber. (allows it to finish `func` execution.)
    my_job_f:shutdown() -- cancells fiber on second call. Fiber will not be restarted even if `restart` is enabled
end
```

## Some examples

### Example: ping fiber

```lua
require 'package.reload'
local json = require 'json'
local metrics = require 'metrics'

ping_f = require 'background' {
    name = 'ping',
    eternal = false, -- allow reload
    wait = 'rw', -- run only on master, and wait until replica became master
    run_interval = 1, -- call `func` each second
    restart = 0.5, -- restart in 500ms after fail of `func`
    setup = function(job)
        job.ping_fail_counter = metrics.counter("ping_fail_counter")
        job.ping_fail_counter = metrics.counter("ping_ok_counter")
    end,
    args = { { timeout = 1, headers = { ["X-Client"] = "Tarantool/ping" } } },
    func = function(job, opts)
        for _, srv in box.space.servers:pairs() do
            local r = http.put(srv.url, json.encode{ id = box.info.id }, opts)
            if r.status == 200 then
                job.ping_ok_counter:inc(1, { url = srv.url })
            end
        end
    end,
    on_fail = function(job, err)
        log.error("Ping func failed: %s", err)
        job.ping_fail_counter:inc(1)
    end,
}
```

### Example: sync fiber

```lua
local json = require 'json'

sync_f = require 'background' {
    name = 'sync',
    args = { "http://upstream:80/api/v1/download", { timeout = 10 } },
    run_interval = 60,
    restart = 1,
    wait = 'rw',
    func = function(job, url, opts)
        local data = http.get(url, opts)
        local res = json.decode(data)

        local n_ins, n_del, n_prds = box.atomic(function()
            local seen = {}
            local n_inserted = 0
            for _, p in ipairs(res.products) do
                seen[p.id] = true
                if not box.space.products:get{p.id} then
                    box.space.products:insert(p)
                    n_inserted = n_inserted + 1
                end
            end

            local n_deleted = box.space.product
                :pairs()
                :grep(function(p) return not seen[p.id] end)
                :map(function(p) return box.space.products:delete{p.id} end)
                :length()

            return n_inserted, n_deleted, #res.products
        end)

        log.info("Downloaded from %s: %s, inserted: %s, deleted: %s", url, n_prds, n_ins, n_del)
    end,
}
```

### Example only replica reloadable fiber

```lua
require 'package.reload'

-- Default run_while callback checks box.info.ro if `wait` option is set.
-- If node became RW without reload job will start waiting to become RO again.
-- In such cast job will be able to be stopped only from outside (replica_f:shutdown() or package.reload())
local replica_f = background {
    name = 'replica_only',
    wait = 'ro',
    restart = true, -- restart on any fail
    eternal = false, -- make it reloadable
    run_interval = 30, -- run func once per 30 seconds
    restart = 3, -- sleep for 3 seconds after fail before restart
    func = function(replica_f)
        print("I am executed only if node is RO")
    end,
}
```

Custom `run_while` handler for RO-only nodes

```lua
local replica_f = background {
    name = 'replica_only',
    wait = 'ro',
    restart = true,
    run_while = function(job)
        --- assert is the only correct way to check RO/RW or other constraints
        -- if you want your job reloadable and restartable
        assert(box.info.ro, "must be executed on RO node only")
        return job.started_at + 86400 < fiber.time()
    end,
    -- default sleep between executions
    run_interval = 10,
    func = function(job)
        print("I am executed only on replica")
        -- You can change sleep interval from func, returning number (in seconds)
        -- But run_interval option will not be changed!
        return math.random(0, 30)
    end,
}
```

## Example obtain state of previous job

Setting replace and intialize/teardown callbacks allows you to transfer state of current execution to new job.

```lua

local fullscan = background {
    name = 'fullscanner',
    -- we tell background that we want to create job to replace __fullscan_job
    replace = rawget(_G, '__fullscan_job'),
    -- replace_timeout = 60, -- we can set replace timeout
    -- we set initialize callback, to obtain state of replacing job (if was any)
    initialize = function(job, previous_ok, previous_state)
        if previous_ok and previous_state and previous_state.cursor then
            -- we set cursor, to continue previous processing
            job.cursor = previous_state.cursor
        end
    end,
    -- setup callback is called after each restart
    -- we just reset iterator here. (It is not necessary in this example)
    setup = function(job)
        if job.cursor then
            job.iterator = box.space.large_space:pairs({job.cursor}, {iterator="GT"})
        else
            job.iterator = box.space.large_space:pairs()
        end
    end,
    -- func callback is our main work-horse. we just iterate over and over
    -- and on each iteration save processed tuple into job.cursor
    func = function(job)
        local no = 0
        -- we can wrap it inside transaction with simple box.begin/box.commit
        -- if we want to somehow modify data
        for _, tuple in job.iterator do
            no = no + 1
            increment_statistics(tuple)

            job.cursor = tuple
            if no % 100 == 0 then
                break
            end
        end
    end,
    -- teardown is executed only once. and used here to pass it's state to the replacer.
    teardown = function(job)
        -- we return our current state to someone outside
        return { cursor = job.cursor, iterator = job.iterator }
    end
}

-- and we assign to some global variable our job object
-- to obtain it from there when we reload our code
rawset(_G, '__fullscan_job', fullscan)

```
