rocks_servers = {
    "https://rocks.ochaton.me", -- ochaton libs
    "http://moonlibs.github.io/rocks", -- moonlibs libs
    "http://rocks.tarantool.org/", -- tarantool libs
    "http://luarocks.org/repositories/rocks", -- luarocks
}
