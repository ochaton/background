box.cfg{listen=3301}

local log = require 'log'
local background = require 'background'
rawset(_G, 'background', background)

box.schema.user.grant('guest', 'super', nil, nil, {if_not_exists=true})

box.schema.space.create('test', {
	if_not_exists = true,
})

box.space.test:create_index('primary', { if_not_exists = true })
box.space.test:truncate()
box.space.test:alter({ temporary = true })

background {
	name = 'pusher',
	run_interval = 0.001,
	restart = 60,
	func = function(job)
		box.begin()
			local n = math.random(500, 1000)

			for i = 1, n do
				if i % 100 == 0 then
					box.commit()
					job:yield()
					box.begin()
				end
				box.space.test:delete({i})
				box.space.test:replace({i})
			end
		box.commit()
	end,
	on_fail = function(job, err)
		log.error("job failed: %s", err)
	end,
}

require 'console'.start()
os.exit(0)

