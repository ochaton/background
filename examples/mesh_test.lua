local fiber = require "fiber"
box.cfg{listen=3301}

local log = require 'log'
local background = require 'background'
rawset(_G, 'background', background)

box.schema.user.grant('guest', 'super', nil, nil, {if_not_exists=true})

local new_producer = function(wrk, chan)
	background {
		name = 'producer/'..(wrk or '-'),
		args = {chan},
		func = function(job, output_ch)
			local n = math.random(100, 1000)
			for i = 1, n do
				output_ch:put({ msg_id = 1000*job.cycles+i, producer = wrk })
			end

			-- random sleep
			return math.random()
		end
	}
end

local new_consumer = function(wk, ...)
	background {
		name = 'consumer/'..wk,
		args = {...},
		func = function(job, in_chan)
			local t = job:yield(1, in_chan)
			if not t then return 0 end

			local s = 0
			for i = 1, t.msg_id do
				for j = 1, t.producer do
					s = s + i*j
				end
			end
			return 0
		end,
	}
end

local chan = fiber.channel(10000)
for w = 1, 10 do
	new_producer(w, chan)
end

for t = 1, 10 do
	new_consumer(t, chan)
end

require 'console'.start()
os.exit(0)

