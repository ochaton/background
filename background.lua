---@module 'background'
local log = require "log"
local ffi = require "ffi"
local fiber = require "fiber"
local clock = require "clock"
local json  = require "json"

local function pack(...)
    return {n = select("#", ...), ...}
end

---comment
---@param fib Fiber
local _fib_status = function(fib)
    return fib:status()
end

---Returns fiber.status() safely
---@param fib any
---@return "dead"|"running"|"suspended"|"none"
local function safe_status(fib)
    if type(fib) ~= 'userdata' then
        return 'none'
    end
    local ok, status = pcall(_fib_status, fib)
    return ok and status or 'dead'
end

---@return boolean
local function is_in_txn()
    return type(box.cfg) == 'table'
        and (type(box.is_in_txn) == 'cdata' or type(box.is_in_txn) == 'function')
        and box.is_in_txn()
end

local mks = 0.000001
local ms = 0.001
local second = 1
local minute = 60*second
local hour = 60*minute
local day  = 24*hour

---Humanifies fractional seconds duration
---@param sec number
---@return string
local function humanify_time(sec)
    sec = tonumber(sec)
    if not sec then
        return "malformed"
    end
    sec = math.abs(sec)

    if sec >= 1 and sec < 10 then
        return ("%.3fs"):format(sec)
    elseif sec < 1 and sec > ms then
        return ("%.1fms"):format(sec*1000)
    elseif sec <= ms then
        return ("%.1fµs"):format(sec*1000*1000)
    end

    local ndays, nhours, nmins, nsecs = "", "", "", ""
    if sec>day then
        ndays = ("%dd"):format(math.floor(sec/day))
        sec = sec%day
    end
    if sec > hour then
        nhours = ("%dh"):format(math.floor(sec/hour))
        sec = sec%hour
    end
    if sec > minute then
        nmins = ("%dm"):format(math.floor(sec/minute))
        sec = sec%minute
    end
    if sec > second then
        nsecs = ("%ds"):format(math.floor(sec/second))
    end

    return ndays..nhours..nmins..nsecs
end

---Returns fiber.csw()
---@param fib Fiber?
---@return number
local function get_csw(fib)
    if fib then
        local s = safe_status(fib)
        if s == "none" or "dead" then
            return 0
        end
    end
    fib = fib or fiber.self()
    if type(fib.csw) == 'function' then
        return fib:csw()
    else
        return fiber.info({bt=false})[fiber.id()].csw
    end
end

---@operator call: background
local M = {}

---@type background[] list of jobs created by background
M.jobs = {}

---@alias background.state
---| "created" First state when job just has been spawned
---| "replacing" [optional] Separate state when job is replacing previous job (when configured)
---| "initializing" State when job enters :initialize() callback
---| "setuping" State when job enters :setup() callback
---| "waiting" [optional] State when job is waiting for ro/rw (when configured)
---| "checking" State when job enters :run_while() callback (1/3 state of short loop)
---| "executing" State when job enters :func() callback (2/3 state of short loop)
---| "sleeping" State when job enters interruptible sleep between subsequent executions (3/3 state of short loop)
---| "restarting" [optional] State when job failed execution but enters interruptible restart sleep (when configured)
---| "finalizing" State after job exited global loop and enters :teardown() callback
---| "exited" final state, when job has been exited

---Simple way to create background fibers
---@class background
---@field name? string name of job
---@field func fun(job: background, ...: any): number? next_sleep_seconds body of fiber loop
---@field initialize? fun(job: background, prev_state: boolean?, ...: any) initialize
---@field state? background.state state of the job
---called exactly once after previous job was replaced.
---@field setup? fun(job: background) setup function
---@field teardown? fun(job: background): ...:any final callback which is called in the end of fiber lifetime.
---teardown may pass it's state to next generation
---@field on_fail? fun(job:background, err: any) calls when `func` has thrown exception
---@field wait 'rw'|'ro' | false ? possible values
---@field max_slice? { err: number, warn: number } fiber.set_slice timings (slightly backported)
---@field replace? background background job which need to be replaced before starting
---@field replace_timeout? number? (default: 60s) gracefull timeout for shutting down previous job
---@field eternal? boolean (Default: false) if enabled the fiber will not be stopped after package.reload()
---if it will consume cpu more than max_slice.err
---@field restart? boolean|number timeout of restart fiber on fail (default: false)
---@field once? boolean (Default: false) runs fiber loop only once
---@field run_interval? number (Default: 1s) timeout of each successfull executions of fiber loop
---@field args? any[] constant arguments of `func`
---@field last_error? any last error of func execution
---@field fiber? Fiber tarantool fiber
---@field debug? boolean enables some debug logs
---@field setuped_at? number timestamp (seconds, fractional) when current restart was created
---@field created_at? number timestamp (seconds, fractional) when fiber was created
---@field looped_at? number timestamp (seconds, fractional) when fiber was entered main loop
---@field last_func_duration? number measured after job returned from func
---@field cycles? number amount of execs
---@field restarts? number amount of restarts
---@field reload_generation? number package.reload.count of fiber
---@field stop? boolean flag denotes of fiber shutdown phase
---@field sleep_cond? fiber.cond condvar of internal sleep loop
local methods = {}
methods.__index = methods

local id = 0
function methods._id()
    id = id + 1
    return id
end

---Callback is called when execution of fiber-loop is failed
---@param self background
---@param err any
function methods:on_fail(err)
    log.error("Background %s failed with %s", self.name, err)
end

---[internal] executes user callback
---@param func any
---@param ... any
---@return table return_values, number duration
function methods:_safe_call(func, ...)
    self.last_error = nil
    local s = clock.monotonic()
    self.executed_at = s
    local r = pack(pcall(func, ...))
    if is_in_txn() then
        box.rollback()
        self:on_fail("transaction left openned")
        -- on_fail might also leave fiber in transaction (but it's extermly stupid)
        if is_in_txn() then
            box.rollback()
            log.error("duude, you openned transaction in on_fail callback and left it")
        end
    end
    -- we do not check fiber/slice on leaving finalizing state
    -- because after this fiber is just exited
    if fiber.check_slice and self.state ~= 'finalizing' then
        fiber.check_slice()
    end
    local dur = clock.monotonic() - s
    if not r[1] then
        self.last_error = r[2]
        self:on_fail(r[2])
    end
    if dur < 0 then
        log.warn("monotonic duration is less than 0: %.3fs %s", dur, json.encode(self:stats()))
    end
    return r, dur
end

---@alias BackgroundState
---| "created" fiber was created
---| "replacing" job is replacing another job (prepare phase)
---| "initializing" job is in job:initialize() callback
---| "initialized" job has been returned from job:initialize() callback
---| "in_main_loop" job is inside inner loop (uninterraptable)
---| "restarting" job is in restarting sleep (can be waked up with job:notify)
---| "finalizing" job is in job:teardown() callback
---| "setuping" job is in job:setup() callback
---| "setuped" job has been returned from job:setup() callback
---| "waiting" job is waiting for job.wait
---| "checking" job is in job:run_while() callback
---| "executing" job is in job:func() callback
---| "sleeping" job is in interaptable sleep for run_interval

---@class BackgroundStats
---@field state BackgroundState
---@field status "dead"|"suspended"|"running"|"none" extended fiber.status
---@field name string background name
---@field fiber Fiber tarantool fiber
---@field created_at number timestamp (seconds, fractional) of start
---@field cycles number amount of execs
---@field restarts number amount of job restarts
---@field looped_at number timestamp (seconds, fractional) of first loop executed
---@field reload_generation number? package.reload.count of job
---@field running number? time spent in callbacks

---@return BackgroundStats
function methods:stats()
    return {
        state = self.state,
        status = safe_status(self.fiber),
        name = self.name,
        fiber = self.fiber,
        created_at = self.created_at,
        looped_at = self.looped_at,
        setuped_at = self.setuped_at,
        cycles = self.cycles,
        restarts = self.restarts,
        reload_generation = self.reload_generation,
        last_func_duration = math.floor((self.last_func_duration or 0) /mks) * mks,
        csw = get_csw(self.fiber),
        running = self.executed_at and clock.monotonic()-self.executed_at,
    }
end

local JobIsStopped = box.error.new {
    code = 0,
    type = 'BackgroundIsStopped',
    reason = 'Background Job has been stopped',
}
M.JobIsStopped = JobIsStopped

local function test_stopped(job)
    if safe_status(job.fiber) ~= "running" then
        return
    end
    if job:is_stopped() then
        JobIsStopped:raise()
    end
end

---yields job. accepts cond or fiber to sleep on
---@async
---@param time number? defaults to 0
---@param chan fiber.channel?
---@param cond fiber.cond?
---@return any
function methods:yield(time, chan, cond)
    if safe_status(self.fiber) ~= "running" then
        return
    end

    time = time or 0
    test_stopped(self)

    local ret
    if chan then
        ret = chan:get(time)
    elseif cond then
        ret = cond:wait(time)
    else
        ret = self.sleep_cond:wait(time)
    end

    test_stopped(self)
    return ret
end

---Callback is called exactly once before the main loop
---But after previous job was replaced
function methods:initialize()
    log.info("initialize %s", self.name)
end

---Callback setup is called right before entering fiber loop
---Each restart also calls setup callback
function methods:setup()
    log.info("setup %s", self.name)
end

---Callback teardown is called only once right before extermination of the fiber
function methods:teardown()
    log.info("teardown %s", self.name)
end

---Callback run_while is called many times to check whether fiber loop should be finished or not
---
---Default callback checks package.reload and fiber cancellation
function methods:run_while()
    local gen = self.reload_generation
    if gen then
        ---@diagnostic disable-next-line: undefined-field
        return gen == package.reload.count
    end
    if self.stop then
        return false
    end
    if self.wait then
        if self.wait == 'rw' then
            assert(not box.info.ro, "node must be RW")
        elseif self.wait == 'ro' then
            assert(box.info.ro, "node must be RO")
        end
    end
    return true
end

---Checks that job was stopped
---@return boolean # is_stopped
function methods:is_stopped()
    if self.stop then
        return true
    end
    local gen = self.reload_generation
    ---@diagnostic disable-next-line: undefined-field
    if gen and gen < package.reload.count then
        return true
    end
    return false
end

---Method shutdown is called to gracefully or forcely cancel job
---First call sets background.stop flag which is checked inside background.run_while callback
---Second call sends fiber.cancel
---@param force boolean? forces fiber.cancel
function methods:shutdown(force)
    self.sleep_cond:broadcast()
    if not self.stop then
        self.stop = true
        self.on_teardown_chan = fiber.channel(1)
        if not force then return end
    end
    -- double shutdown attempts to cancel fiber
    if self.fiber then
        local ok, err = pcall(self.fiber.cancel, self.fiber)
        if not ok then
            log.error("fiber.cancel for %s failed with: %s", self.name, err)
        end
    end
end

---background:notify is a proper way to wakeup fiber from sleep
function methods:notify()
    self.sleep_cond:broadcast()
end

---Renames background fiber
---@param new_name string
function methods:rename(new_name)
    if type(new_name) ~= 'string' or #new_name == 0 then
        return self.name
    end
    if self.fiber and self.fiber:status() ~= "cancelled" then
        self.fiber:name(new_name, { truncate = true })
        self.name = new_name
    end
    return self.name
end

function methods:__serialize()
    return ("Background<%s%s> {wait:%s,restart:%s,interval:%s} at %s%s"):format(
        self.name,
        self.reload_generation and ("/g%s"):format(self.reload_generation) or '',
        self.wait or '-',
        self.restart,
        self.run_interval,
        os.date('%FT%T', self.created_at),
        self.looped_at and "/"..os.date("%FT%T", self.looped_at) or ""
    )
end

function methods:__tostring()
    return self:__serialize()
end

M.max_slice = { err = 1, warn = 0.5 }

---Spawns new background fiber
---@param _ any
---@param opts background
---@return background
function M.new(_, opts)
    assert(type(opts) == "table", "opts required to be table")
    local self = setmetatable(opts, methods)

    self.name = self.name or ("background/" .. self:_id())
    assert(self.func, "func is required for background")

    self.run_interval = tonumber(self.run_interval) or 1
    self.args = self.args or {}
    self.sleep_cond = fiber.cond()
    if type(self.max_slice) == 'table' then
        self.max_slice.warn = tonumber(self.max_slice.warn)
        self.max_slice.err = tonumber(self.max_slice.err)
    elseif type(self.max_slice) == 'number' then
        local max_slice = self.max_slice --[[@as number]]
        self.max_slice = { err = max_slice, warn = max_slice*0.8 }
    else
        self.max_slice = setmetatable({},{__index=M.max_slice})
    end

    self.fiber = fiber.create(self.fiber_f, self)
    return self
end

---@param self background
local function loop_f(self)
    self.state = 'setuping'
    local r = self:_safe_call(self.setup, self)
    if not r[1] then
        return "stop", "setup failed"
    end

    self.setuped_at = fiber.time()
    self.cycles = 0

    if self.wait then
        if self.wait == 'rw' and box.info.ro then
            log.warn("Entering wait loop. Need to be run on RW but node is RO")
        elseif self.wait == 'ro' and not box.info.ro then
            log.warn("Entering wait loop. Need to be run on RO but node is RW")
        end
    end

    while self.wait and not self:is_stopped() do
        self.state = 'waiting'
        local wait_for, wait_with
        if self.wait == "rw" then
            wait_for = not box.info.ro
            wait_with = box.ctl and box.ctl.wait_rw or fiber.sleep
        elseif self.wait == "ro" then
            wait_for = box.info.ro
            wait_with = box.ctl and box.ctl.wait_ro or fiber.sleep
        else
            break
        end

        if wait_for then
            break
        end

        pcall(wait_with, 0.01)
        test_stopped(self)
    end

    -- Enter loop:
    self.looped_at = fiber.time()
    while not self:is_stopped() do
        self.state = 'checking'
        if not self:run_while() then
            return "stop", "run_while returned false"
        end
        self.cycles = self.cycles + 1
        self.state = 'executing'
        local res, dur = self:_safe_call(self.func, self, unpack(self.args or {}))
        local executed_ok = res[1]
        local sleep = tonumber(res[2]) or self.run_interval
        if self.debug then
            log.info("cycle=%s was executed %q (took %s, next_run: %s)",
                self.cycles,
                executed_ok and "ok" or "bad",
                humanify_time(dur),
                humanify_time(sleep)
            )
        end
        self.last_func_duration = dur
        if not executed_ok then
            return "cont", "func failed"
        end
        if self.once then
            return "stop", "once execution successfully finished"
        end
        self.state = 'checking'
        if not self:run_while() then
            return "stop", "run_while returned false"
        end
        self.state = 'sleeping'
        self:yield(sleep)
        fiber.testcancel()
    end
end


---@param job background
local function job_is_down(job)
    local s = safe_status(job)
    return job:is_stopped()
        and (not job.fiber or (s == "dead" or s == "none"))
end

---Fiber function
function methods:fiber_f()
    self.state = 'created'

    self.fiber = fiber.self()
    fiber.name(self.name, {truncate = true})
    log.info("Starting background job: wait:%s restart:%s run_interval:%s once:%s eternal:%s",
        self.wait, self.restart, self.run_interval, self.once, self.eternal)


    ---@diagnostic disable-next-line: undefined-field
    if package.reload and not self.eternal then
        ---@diagnostic disable-next-line: undefined-field
        self.reload_generation = package.reload.count
    end

    self.restarts = 0
    self.created_at = fiber.time()

    local prev_job = self.replace
    if prev_job
        and type(prev_job) == 'table'
        and type(prev_job.shutdown) == 'function'
        and type(prev_job.is_stopped) == 'function'
    then
        local shutdown_timeout = tonumber(self.replace_timeout) or 60
        self.state = 'replacing'

        while not job_is_down(prev_job) do
            prev_job:shutdown()
            local fin_chan = prev_job.on_teardown_chan
            local deadline = fiber.time()+shutdown_timeout

            if fin_chan then
                local prev_state = self:yield(shutdown_timeout, fin_chan)
                if prev_state then
                    self.prev_state = prev_state
                end
            else
                while fiber.time() < deadline do
                    if job_is_down(prev_job) then break end
                    self:yield(ms)
                end
            end
        end
        -- yes, that's ackward but we must close channel not them :(
        -- because if them close channel then we do not receive message
        if prev_job.on_teardown_chan then
            prev_job.on_teardown_chan:close()
        end
        log.info("previous job is down: %s entering new job", prev_job)
        self.replace = nil
    end

    if fiber.set_max_slice then
        fiber.set_max_slice(self.max_slice)
    end

    self.state = 'initializing'
    do
        local prev_state = self.prev_state or {n=0}
        self:_safe_call(self.initialize, self, unpack(prev_state, 1, prev_state.n))
    end

    while not self:is_stopped() do
        fiber.testcancel()
        local _, err, reason = pcall(loop_f, self)

        if is_in_txn() then
            log.error("Job left openned transaction, executing box.rollback()")
            box.rollback()
        end

        local exit_reason

        if err == "stop" then
            log.warn("Finishing background because of %s", reason)
            self.exit_reason = reason
            break
        elseif err == "cont" then
            exit_reason = reason -- can be exit_reason
            log.warn("Background failed because: %s", reason)
        elseif ffi.istype("struct error", err) then
            ---@cast err +BoxErrorObject
            if err.type == "FiberIsCancelled" then
                log.warn("Fiber was cancelled. Finishing")
                self.exit_reason = err
                break
            elseif err == JobIsStopped then
                log.warn("Job was stopped %s", err)
                self.exit_reason = err
                break
            elseif err.type == 'FiberSliceIsExceeded' then
                log.warn("Job will be terminated because it consumes too much cpu: %s", err)
                self.exit_reason = err
                break
            else
                log.warn("Caught box.error: %s", err)
                exit_reason = err
            end
        elseif err then
            log.error("Caught unknown error: %s", err)
            exit_reason = err
        end

        if not self.restart or self:is_stopped() then
            log.warn("Leaving background loop")
            self.exit_reason = exit_reason or self.last_error
            break
        end

        self.state = 'restarting'
        self:yield(tonumber(self.restart) or 1)
        log.info("Restarting background job")
        self.restarts = self.restarts + 1
    end
    if not self.exit_reason then
        self.exit_reason = self:is_stopped() and 'stopped' or 'unknown'
    end

    self.state = 'finalizing'
    local r = self:_safe_call(self.teardown, self)
    if self.on_teardown_chan then
        self.on_teardown_chan:put(r, 0)
    end
    self.state = 'exited'
end

return setmetatable(M, {__call = M.new})
