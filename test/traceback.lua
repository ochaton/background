local static_msg = {}

local mt = {
	__tostring = function(stack)
		local s = tostring(stack.error)..'\nstack traceback:'
		for _, frame in ipairs(stack.trace) do
			s = s .. '\n\t'..frame.message
		end
		return s
	end,
}

local function trace(lvl)
	lvl = lvl+1

	local stack = {}
	while true do
		local inf = debug.getinfo(lvl, "Snl")
		lvl = lvl + 1
		if not inf then break end

		local frame = {
			source = inf.source:sub(2),
			currentline = inf.currentline,
			linedefined = inf.linedefined,
			namewhat = inf.namewhat,
			what = inf.what,
			name = inf.name,
		}

		table.insert(static_msg, frame.source)
		table.insert(static_msg, ':')

		if frame.currentline > 0 then
			table.insert(static_msg, frame.currentline)
			table.insert(static_msg, ':')
		end

		if frame.namewhat ~= "" then
			table.insert(static_msg, (" in function '%s'"):format(frame.name))
		elseif frame.what == 'main' then
			table.insert(static_msg, " in main chunk")
		elseif frame.what == 'C' or frame.what == 't' then
			table.insert(static_msg, " ?")
		else
			table.insert(static_msg, (" in function <%s:%d>"):format(frame.source, frame.linedefined))
		end

		frame.message = table.concat(static_msg)
		table.clear(static_msg)

		table.insert(stack, frame)
	end

	return stack
end

local function traceback(err, lvl)
	return setmetatable({
		trace = trace((lvl or 1)+1),
		error = err,
	}, mt)
end

local function traceback_str(err, lvl)
	return mt.__tostring({
		trace = trace((lvl or 1)+1),
		error = err,
	})
end

return {
	traceback = traceback,
	traceback_str = traceback_str,
}