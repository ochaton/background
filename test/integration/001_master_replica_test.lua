local t = require 'luatest'
local g = t.group 'master_replica'

local fiber = require 'fiber'
local helper = require 'test.integration.helper'

g.before_each(helper.before_suite)
g.after_each(helper.after_suite)

g.test_master_fiber = function()
	helper.promote_node("instance_001")

	local master = helper.server.instance_001
	local r
	r = master:call('dostring', {"return app.is_master"})
	t.assert_equals(r, true)

	r = master:call('dostring', {"return app.is_replica"})
	t.assert_equals(r, false)

	r = master:call('dostring', {"return app.now"})
	t.assert_lt(math.abs(tonumber(r-fiber.time())), 2)

	r = master:call('dostring', {"return app.counter"})
	t.assert_gt(r, 0)

	r = master:call('dostring', {"return app.master_job_run"})
	t.assert_is(r, true, "master_job is running")

	r = master:call('dostring', {"return app.replica_job_run"})
	t.assert_is_not(r, true, "replica_job is not running")

	r = master:call('dostring', {"return app.ping_job_run"})
	t.assert_is(r, true, "ping_job is running")
end