local t = require 'luatest'

local fio = require 'fio'
local log = require 'log'

local helper = {
	---@type table<string,luatest.server>
	server = {}
}

local root = fio.dirname(fio.abspath(debug.getinfo(1, "S").source:sub(2)))
helper.server_datadir = fio.pathjoin(root, 'mock', 'server', 'tmp')
helper.server_rocksdir = fio.pathjoin(root, 'mock', 'server', '.rocks')
helper.server_root    = fio.pathjoin(root, 'mock', 'server')
helper.server_script  = fio.pathjoin(helper.server_root, 'init.lua')

helper.server_names = {
	instance_001 = { listen_port = 3301 },
	instance_002 = { listen_port = 3302 },
}


function helper.clear_dir()
	log.info("removing: %s", helper.server_datadir)
	fio.rmtree(helper.server_datadir)
	log.info("removing: %s", helper.server_rocksdir)
	fio.rmtree(helper.server_rocksdir)
end

function helper.install_mock_dependencies()
	log.info("installing config 0.6.3 to %s", helper.server_rocksdir)
	local cmd = "tarantoolctl rocks --server https://moonlibs.org --tree "..helper.server_rocksdir.." install config 0.6.3"
	for line in io.popen(cmd):lines() do
		print(line)
	end
end

function helper.promote_node(instance_name)
	local master = assert(helper.server[instance_name], "no node")
	for name, srv in pairs(helper.server) do
		if name ~= instance_name then
			srv:call('dostring', {"box.cfg{read_only=true}"})
		end
	end

	master:call('dostring', {"box.cfg{read_only=false}"})
end

function helper.before_suite()
	helper.clear_dir()
	helper.install_mock_dependencies()
	for name, node_cfg in pairs(helper.server_names) do
		helper.server[name] = t.Server:new({
			command = helper.server_script,
			workdir = helper.server_datadir,
			chdir = helper.server_root,
			env = {
				TT_INSTANCE_NAME = name,
			},
			net_box_port = node_cfg.listen_port,
			net_box_credentials = {
				user = 'guest',
				password = ''
			}
		})
	end

	helper.server_start()
	helper.server_wait_available()
	log.info("Server is available")
end

function helper.after_suite()
	helper.server_stop()
	require'fiber'.sleep(0.5)
	helper.clear_dir()
end

function helper.server_start()
	log.info("Starting server")
	for node, srv in pairs(helper.server) do
		log.info("Starting: %s", node)
		srv:start()
	end
	log.info("Server started")
end

function helper.server_stop()
	log.info("Stoping server")
	for _, srv in pairs(helper.server) do
		srv:stop()
	end
end

function helper.server_wait_available()
	for name, srv in pairs(helper.server) do
		t.helpers.retrying({ timeout = 10 }, function() srv:connect_net_box() end)
		t.helpers.retrying({ timeout = 10 }, function()
			assert(srv:call('config.get', {"etcd.instance_name"}) == name)
		end)
	end
end

return helper