local M = {}

local fiber = require 'fiber'

M.ping_job = require 'background' {
	name = 'ping_f',
	restart = true,
	wait = false,
	run_interval = 1,
	setup = function(job)
		job.module = M
		job.counter = 0
	end,
	func = function(job)
		job.module.now = fiber.time()
		job.counter = job.counter + 1
		job.module.counter = job.counter
		job.module.ping_job_run = true
	end,
	teardown = function(job)
		job.module.ping_job_run = false
	end,
}

M.master_job = require 'background' {
	name = 'master_f',
	restart = true,
	wait = 'rw',
	run_interval = 1,
	setup = function(job)
		job.module = M
	end,
	run_while = function() return not box.cfg.read_only end,
	func = function(job)
		job.module.is_replica = false
		job.module.is_master = true
		job.module.master_job_run = true
	end,
	teardown = function(job)
		job.module.is_master = box.cfg.read_only == false
		job.module.master_job_run = false
	end,
}

M.replica_job = require 'background' {
	name = 'replica_f',
	restart = true,
	wait = 'ro',
	run_interval = 1,
	setup = function(job)
		job.module = M
	end,
	run_while = function() return box.cfg.read_only end,
	func = function(job)
		job.module.is_replica = true
		job.module.is_master = false
		job.module.replica_job = true
	end,
	teardown = function(job)
		job.module.is_replica = box.cfg.read_only == true
		job.module.replica_job = false
	end,
}

return M