local t = require 'luatest'
local clock = require 'clock'
local helper = require 'test.unit.helper'
local fio    = require 'fio'

---@class test.background.group.basic:luatest.group
local g = t.group 'basic'

local fiber = require 'fiber'
local background = require 'background'

g.before_all(function()
	debug.traceback = require 'test.traceback'.traceback_str
end)

g.test_that_job_is_background = function()
	local fib = fiber.self()
	local chan = fiber.channel(1)
	local job = background:new {
		func = function()
			t.assert_is_not(fib:id(), fiber.id(), "job is running in separate fiber")
			chan:put(fiber.id(), 0)
		end,
	}

	t.assert(job, "background job is running")
	t.assert_equals(job.run_interval, 1, "job default run_interval is 1")
	t.assert(not job.restart,"job restart is not defined")
	t.assert(not job.once,"job once is not defined")
	t.assert(not job.wait,"job wait is not defined")
	t.assert(not job.eternal,"job eternal is not defined")
	t.assert(not job.replace,"job replace is not defined")
	t.assert_equals(job.args, {}, "job args is empty table")

	t.assert_equals(chan:get(1), job.fiber:id(), "job.fiber is equal to background func fiber")
	t.assert_ge(job.cycles, 1, "job was executed more than 1 time")

	chan:close()
	job:shutdown()
	fiber.yield()

	t.assert_equals(job.fiber:status(), "dead", "job fiber is dead after shutdown")
end

g.test_that_job_is_restarted = function()
	local err = false
	local success = 0

	local job = background:new {
		restart = 1,
		run_interval = 0.1,
		func = function()
			if err then error("injected") end
			success = success + 1
		end
	}

	t.assert(job, "is running")
	t.assert_type(job.cycles, 'number', "job.cycles is a number")

	--- wait until job.cycles becomes more than 1
	t.helpers.retrying({}, function()
		assert(job.cycles > 1)
	end)

	t.assert_gt(job.cycles, 1, "job.cycles must be > 1")

	local prev_success = success
	t.helpers.retrying({}, function()
		assert(success > prev_success)
	end)

	t.assert_gt(success, prev_success, "job is cycling normally")

	local looped_at = job.looped_at
	local restarts = job.restarts
	err = true

	t.helpers.retrying({ timeout = 2*job.run_interval + job.restart, delay = job.restart / 10 }, function()
		if (job.restarts <= restarts) then
			error("not enough restarts")
		end
	end)

	err = false

	t.assert_gt(job.restarts, restarts, "job has been restarted")
	t.assert_gt(job.cycles, 0, "job is cycling")
	t.assert_gt(job.looped_at, looped_at, "job.looped_at was increased")

	local cycles = job.cycles

	prev_success = success
	t.helpers.retrying({ timeout = 2*job.run_interval + job.restart, delay = job.restart / 10 }, function()
		assert(success > prev_success)
	end)

	t.assert_gt(success, prev_success, "after restart success was increased")

	t.helpers.retrying({ timeout = 2*job.run_interval, delay = job.run_interval }, function()
		assert(job.cycles > cycles)
	end)

	t.assert_gt(job.cycles, cycles, "job is cycling")

	job:shutdown()
end

g.test_that_once_job_is_restarted_when_failed = function()

	local on_fail_called = 0
	local completed = 0
	local terminated = false

	local job = background:new {
		name = 'once-fail-ok',
		once = true,
		restart = true,
		args = { {num=2} },
		func = function(_, arg)
			arg.num = arg.num-1
			if arg.num > 0 then
				error("fail")
			end
			completed = completed+1
		end,
		on_fail = function()
			on_fail_called = on_fail_called+1
		end,
		teardown = function()
			terminated = true
		end,
	}

	t.assert(job, "background job is running")
	t.assert_equals(job.run_interval, 1, "job default run_interval is 1")
	t.assert(job.restart,"job restart is defined")
	t.assert(job.once,"job once is defined")
	t.assert(not job.wait,"job wait is not defined")
	t.assert(not job.eternal,"job eternal is not defined")
	t.assert(not job.replace,"job replace is not defined")

	--- wait until on_fail_called becomes more than 0
	t.helpers.retrying({}, function()
		assert(on_fail_called > 0)
	end)

	-- wait until completed becomes more than 0
	t.helpers.retrying({}, function()
		assert(completed > 0)
	end)

	-- wait until job is terminated
	t.helpers.retrying({}, function()
		assert(terminated)
	end)

	t.assert_equals(job.restarts, 1, "job must be restarted exactly once")
	t.assert(terminated, "job has been terminated")
	t.assert_equals(on_fail_called, 1, "job calls on_fail exactly once")
	t.assert_equals(completed, 1, "job completed execution exactly once")
end

g.test_that_job_properly_replacing_another_job = function()
	local counter = 0
	local job = background:new {
		func = function(ctx)
			counter = counter+1
			ctx.counter = counter
		end,
		teardown = function(ctx)
			return { terminated = clock.time(), counter = ctx.counter }
		end,
	}

	t.assert(job, "background job is running")
	t.assert_equals(job.run_interval, 1, "job default run_interval is 1")
	t.assert(not job.restart,"job restart is not defined")
	t.assert(not job.once,"job once is not defined")
	t.assert(not job.wait,"job wait is not defined")
	t.assert(not job.eternal,"job eternal is not defined")
	t.assert(not job.replace,"job replace is not defined")

	t.helpers.retrying({}, function()
		assert(job.cycles > 1)
	end)

	local chan = fiber.channel()
	local new_job = background:new {
		initialize = function (_, prev_state, ctx)
			chan:put({prev_state, ctx})
		end,
		func = function () end,
		once = true,
		replace = job,
	}

	local prev_state, ctx = unpack(chan:get())

	t.assert_equals(ctx.counter, counter, 'state must be transfered')
	t.assert_gt(clock.time(), ctx.terminated, 'this callback must be executed after previous job terminated')
	t.assert_equals(job.fiber:status(), 'dead', 'fiber of previous job must be dead')
	t.assert_equals(job.exit_reason, background.JobIsStopped, 'previous job exit reason is stopped')
	t.assert(prev_state, 'teardown exited ok')

	t.helpers.retrying({}, function ()
		assert(new_job.state == 'exited')
	end)
end

g.test_that_job_is_waiting_for_ro = function()
	local tmpdir = helper.create_temp_dir()
	t.after_suite(helper.once(function() -- ? is it good idea to abuse after_suite hooks like that
		assert(fio.rmtree(tmpdir))
	end))

	local background_path = fio.dirname(assert(package.search('background')))

	local server = t.Server:new{
		alias = 'test_ro',
		workdir = tmpdir,
		coverage_report = true,
	}

	server:start()

	t.helpers.retrying({ timeout = 10, delay = 3 }, function ()
		server:connect_net_box()
	end)

	server:exec(function(dir_path)
		-- ! this function must not have any upvalues
		package.path = package.path..';'..dir_path..'/?.lua'

		local global = {ro = { first = nil, last = nil }, rw = { first = nil, last = nil }}
		rawset(_G, 'global', global)

		global.ro.job = require 'background':new{
			wait = 'ro',
			restart = 0.1,
			func = function()
				if not global.ro.first then
					global.ro.first = box.info()
				end
				global.ro.last = box.info()
			end
		}

		global.rw.job = require 'background':new{
			wait = 'rw',
			restart = 0.1,
			func = function()
				if not global.rw.first then
					global.rw.first = box.info()
				end
				global.rw.last = box.info()
			end,
		}

	end, {background_path})

	local info_ro = server:exec(function() return box.info.ro end)
	t.assert_is(info_ro, false, 'tarantool must be started as rw')

	local glob = server:exec(function() return rawget(_G, 'global') end)
	t.assert_type(glob, 'table', 'global must be table')

	t.assert_is(glob.ro.first, nil, 'ro job must not be executed yet')
	t.assert_is(glob.ro.last, nil, 'ro job must not be executed yet')

	t.assert_is_not(glob.rw.first, nil, 'rw job must already be executed yet')
	t.assert_is_not(glob.rw.last, nil, 'rw job must already be executed yet')

	-- ! RW -> RO
	server:exec(function() box.cfg{read_only=true} end)

	glob = server:exec(function() return rawget(_G, 'global') end)
	t.assert_type(glob, 'table', 'global must be table')

	t.assert_type(glob.ro.first, 'table', 'ro job must be already executed')
	t.assert_is(glob.ro.first.ro, true, 'box.info.ro must be true')

	t.assert_type(glob.ro.last, 'table', 'ro job must be already executed')
	t.assert_is(glob.ro.last.ro, true, 'box.info.ro must be true')

	t.assert_type(glob.rw.last, 'table', 'rw job must be already executed')
	t.assert_is(glob.rw.last.ro, false, 'rw job keeps only box.info.ro false as last state')

	-- ! RO -> RW
	server:exec(function() box.cfg{read_only=false} end)

	local glob2 = server:exec(function()
		local state = rawget(_G, 'global')
		state.ro.job:notify()
		state.rw.job:notify()

		require'fiber'.sleep(1.2)
		return state
	end)

	t.assert_type(glob2, 'table', 'global must be table')
	t.assert_type(glob2.ro.last, 'table', 'last not be changed')

	t.assert_is(glob2.ro.last.ro, true, 'ro job box.info.ro must be true')
	t.assert_is(glob2.ro.last.uptime, glob.ro.last.uptime, 'last execution must not change')

	t.assert_is(glob2.rw.last.ro, false, 'rw job box.info.ro must be true')
	t.assert_gt(glob2.rw.last.uptime, glob.rw.last.uptime, 'rw last cycles must increase')

	server:stop()
end

g.test_that_cpu_consuming_jobs_are_unlooped = function()
	t.skip_if(not fiber.check_slice, 'this version of tarantool does not support fiber.check_slice')
	local job = background:new{
		name = 'cpu_consuming',
		max_slice = { warn = 0.1, err = 0.5 },
		func = function (job)
			job:yield(10) -- sleep time is doesn't matter
			local d = clock.time()+0.75
			while clock.time()<d do end
		end
	}

	t.assert_covers(job:stats(), {
		state = 'executing',
		status = 'suspended',
		name = 'cpu_consuming',
		cycles = 1,
		restarts = 0,
	}, 'job stats')

	t.assert(job, "background job is running")
	t.assert_equals(job.run_interval, 1, "job default run_interval is 1")
	t.assert(not job.restart,"job restart is not defined")
	t.assert(not job.once,"job once is not defined")
	t.assert(not job.wait,"job wait is not defined")
	t.assert(not job.eternal,"job eternal is not defined")
	t.assert(not job.replace,"job replace is not defined")
	t.assert_equals(job.args, {}, "job args is empty table")
	t.assert_is(job.cycles, 1, "job is cycling")
	t.assert_is(job.state, 'executing', 'job is inside func')

	job:notify()
	fiber.yield() -- we yield execution to the job

	t.assert_is(job.state, 'exited', 'job must be exited')
	t.assert_type(job.exit_reason, 'cdata', 'job preserved exit_reason')
	t.assert_is(job.exit_reason.type, 'FiberSliceIsExceeded', 'job was exited with FiberSliceIsExceeded')

	t.assert_covers(job:stats(), {
		state = 'exited',
		status = 'dead',
		name = 'cpu_consuming',
		restarts = 0,
		cycles = 1,
	}, 'job stats')
end

g.test_that_background_closes_transactions = function()

	local txns = {}
	local states = {}
	box.cfg{}

	local job = background:new{
		restart = 1,
		initialize = function()
			txns[#txns+1] = box.is_in_txn()
			states[#states+1]='init'
			box.begin()
		end,
		setup = function()
			txns[#txns+1] = box.is_in_txn()
			states[#states+1]='setup'
			box.begin()
		end,
		func = function()
			txns[#txns+1] = box.is_in_txn()
			states[#states+1]='func'
			box.begin()
		end,
		on_fail = function()
			txns[#txns+1] = box.is_in_txn()
			states[#states+1]='on_fail'
			box.begin()
		end,
		teardown = function()
			txns[#txns+1] = box.is_in_txn()
			states[#states+1]='teardown'
			box.begin()
		end,
	}

	job:notify()
	job:shutdown()
	fiber.yield()

	local n = #txns
	local falses = require'fun'.range(1, n):map(function() return false end):totable()

	t.assert_covers(txns, falses, ('each callback of background is executed outside any transaction: "%s"'):format(
		table.concat(states, ',')
	))
end
