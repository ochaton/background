local t = require 'luatest'

local fio = require 'fio'
local log = require 'log'

local helper = {
	server = {}
}

local root = fio.dirname(fio.abspath(debug.getinfo(1, "S").source:sub(2)))
helper.server_datadir = fio.pathjoin(root, 'mock', 'server', 'tmp')
helper.server_rocksdir = fio.pathjoin(root, 'mock', 'server', '.rocks')
helper.server_root    = fio.pathjoin(root, 'mock', 'server')
helper.server_script  = fio.pathjoin(helper.server_root, 'init.lua')

---Creates tempdir
---@return string
function helper.create_temp_dir()
	local tmpdir = fio.tempdir()
	fio.mkdir(tmpdir)
	return tmpdir
end

function helper.clear_dir()
	log.info("removing: %s", helper.server_datadir)
	fio.rmtree(helper.server_datadir)
	log.info("removing: %s", helper.server_rocksdir)
	fio.rmtree(helper.server_rocksdir)
end

function helper.once(callback, ...)
	local executed = false
	return function(...)
		if executed then return end
		executed = true
		return callback(...)
	end, ...
end

function helper.install_mock_dependencies()
	log.info("installing config 0.6.1 to %s", helper.server_rocksdir)
	local cmd = "tarantoolctl rocks --server https://moonlibs.org --tree "..helper.server_rocksdir.." install config 0.6.1"
	for line in io.popen(cmd):lines() do
		print(line)
	end
end

function helper.promote_node(instance_name)
	local master = assert(helper.server[instance_name], "no node")
	for name, srv in pairs(helper.server) do
		if name ~= instance_name then
			srv:call('dostring', {"box.cfg{read_only=true}"})
		end
	end

	master:call('dostring', {"box.cfg{read_only=false}"})
end

function helper.server_start()
	log.info("Starting server")
	for node, srv in pairs(helper.server) do
		log.info("Starting: %s", node)
		srv:start()
	end
	log.info("Server started")
end

function helper.server_stop()
	log.info("Stoping server")
	for _, srv in pairs(helper.server) do
		srv:stop()
	end
end

function helper.server_wait_available()
	for name, srv in pairs(helper.server) do
		t.helpers.retrying({ timeout = 10 }, function() srv:connect_net_box() end)
		t.helpers.retrying({ timeout = 10 }, function()
			assert(srv:call('config.get', {"etcd.instance_name"}) == name)
		end)
	end
end

return helper