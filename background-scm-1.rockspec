package = "background"
version = "scm-1"
source = {
   url = "git+https://gitlab.com/ochaton/background.git"
}
description = {
   homepage = "https://gitlab.com/ochaton/background",
   summary = "The proper way to use Tarantool background fibers",
   license = "WTFPL"
}
build = {
   type = "builtin",
   modules = {
      background = "background.lua"
   }
}
